# gbif-images

This repository grabs images from GBIF based on an occurrence.txt and multimedia.txt file obtained from a GBIF data download.

## Getting started

1. Download the data from GBIF
2. Unzip the data into the input folder
3. Set up a `poetry` environment and install the project
4. Run `poetry run python ./scripts/script.py`
