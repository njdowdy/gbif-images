import pandas as pd
import requests

def extract_urls():
    df = pd.read_csv("./input/multimedia.txt", sep="\t", usecols=['gbifID', 'identifier'])
    urls = df.to_dict('records')
    return urls

def extract_occurrences():
    df = pd.read_csv("./input/occurrence.txt", sep="\t", usecols=['gbifID', 'genus', 'specificEpithet', 'stateProvince'])
    data = df.to_dict('records')
    return data

# def extract_genera():
#     df = pd.read_csv("./input/occurrence.txt", sep="\t", usecols=['genus'])
#     genera = df['genus'].unique()
#     with open('./input/genera.txt', 'w') as file:
#         for genus in genera:
#             file.write(f"{genus}\n")
#     return genera

def merge_data(urls, data):
    for record in data:
        for url in urls:
            if url['gbifID'] == record['gbifID']:
                if(not pd.isna(record['stateProvince'])):
                    url['stateProvince'] = record['stateProvince']
                else:
                    url['stateProvince'] = ''
                if(not pd.isna(record['specificEpithet'])):
                    url['genus'] = record['genus']
                else:
                    url['genus'] = 'Unknown'
                if(not pd.isna(record['specificEpithet'])):
                    url['specificEpithet'] = record['specificEpithet']
                else:
                    url['specificEpithet'] = 'sp'
                # TODO: Look up higher taxonomy of genus name and set url['subfamily'], url['tribe'], url['subtribe']
    # deduplicate species
    new_urls = deduplicate_records(urls)
    return urls

def deduplicate_records(urls):
    name = []
    new_urls = []
    for url in urls:
        if f"{url['genus']}_{url['specificEpithet']}" not in name:
            name.append(f"{url['genus']}_{url['specificEpithet']}")
            new_urls.append(url)
    return new_urls

def get_image_data(url):
    # Send a GET request to the URL
    response = requests.get(url['identifier'])

    # Check if the request was successful (HTTP status code 200)
    if response.status_code == 200:
        # Open a file in binary write mode in the specified output path
        fileName = f"./output/Panama_{url['stateProvince']}_{url['genus']}_{url['specificEpithet']}_{url['gbifID']}.jpg"
        with open(fileName, 'wb') as file:
            # Write the content of the response (the image data) to the file
            file.write(response.content)
    else:
        print(f"Failed to download image. HTTP status code: {response.status_code}")

def main():
    urls = extract_urls()
    # deduplicate species
    data = extract_occurrences()
    res = merge_data(urls, data)

    for url in res:
        get_image_data(url)
    print(res)


if __name__ == "__main__":
    main()
